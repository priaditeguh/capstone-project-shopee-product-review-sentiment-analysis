# Product Reviews - Sentiment Analysis #

For this sentiment analysis project, we choose a product review dataset from Shopee, one of the leading online shop companies in South East Asia. The dataset we obtain is from Shopee Code League 2020 competition. They organize the competition in Kaggle, so we can obtain the dataset in [Shopee Code League - Sentiment Analysis](https://www.kaggle.com/c/shopee-sentiment-analysis/overview) Kaggle page.

Since the dataset was collectedin the South East Asia market, many reviews are mixed with Indonesia words.  Soinstead  of  translating  those  Indonesian  words,  we  use  a  multilingual  model,  i.e. [XLM-RoBERTa](https://huggingface.co/transformers/model_doc/xlmroberta.html). 

## Dataset ##

You can obtain the full dataset [here](https://www.kaggle.com/teguhwibowo/shopeesentimentanalysis)

## Library ##

We use [Hugging Face](https://github.com/huggingface/transformers) to use as the transformer-based NLP model. You can install it simply by typing `pip install transformers`.

## Results ##

Our model achieve 0.67098 accuracy. Below is the plot loss value for every epoch. 

![Loss Plot](image/loss_plot.png)
